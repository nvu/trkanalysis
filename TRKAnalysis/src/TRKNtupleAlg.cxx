#include "TRKNtupleAlg.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackStateValidationContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthPileupEventContainer.h"

#include "TrkTrack/TrackStateOnSurface.h"
#include "TrkEventPrimitives/TrackStateDefs.h"

#include "TMath.h"
#include "TVector3.h"

#include "CachedGetAssocTruth.h"


namespace{

  bool acceptVertex(const xAOD::Vertex* vtx) {
    return(vtx->vertexType() != xAOD::VxType::NoVtx);
  }

  bool
  isInsideOut(const xAOD::TrackParticle& track) {
    std::bitset<xAOD::TrackPatternRecoInfo::NumberOfTrackRecoInfo>  patternInfo = track.patternRecoInfo();
    return patternInfo.test(0);
  }



}



TRKNtupleAlg::TRKNtupleAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){
  declareProperty("TrackParticleContainerName", m_trkParticleName = "InDetTrackParticles");
  declareProperty("TruthParticleContainerName", m_truthParticleName = "TruthParticles");
  declareProperty("TruthVertexContainerName", m_truthVertexContainerName = "TruthVertices");
  declareProperty("VertexContainerName", m_vertexContainerName = "PrimaryVertices");
  declareProperty("TruthEventContainerName", m_truthEventName = "TruthEvents");
  declareProperty("PileupEventContainerName", m_pileupEventName = "TruthPileupEvents");
  declareProperty("IsHitInfoAvailable", m_isHitInfoAvailable = false);
}


TRKNtupleAlg::~TRKNtupleAlg() {}


StatusCode TRKNtupleAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
 
  m_myTree = new TTree("TRKTree","TRKTree");
  CHECK( histSvc()->regTree("/MYSTREAM/TRKTree", m_myTree) ); //registers tree to output stream

  m_myTree->Branch("event",&_EventNumber,"event/L");

  m_myTree->Branch("track_n",&_track_n,"track_n/I");
  m_myTree->Branch("track_isLoose",&_track_isLoose);
  m_myTree->Branch("track_isTightPrimary",&_track_isTightPrimary);
  m_myTree->Branch("track_isInOut",&_track_isInOut);
  m_myTree->Branch("track_charge",&_track_charge);
  m_myTree->Branch("track_pt",&_track_pt);
  m_myTree->Branch("track_eta",&_track_eta);
  m_myTree->Branch("track_theta",&_track_theta);
  m_myTree->Branch("track_phi",&_track_phi);
  m_myTree->Branch("track_phi0",&_track_phi0);
  m_myTree->Branch("track_d0",&_track_d0);
  m_myTree->Branch("track_z0",&_track_z0);
  m_myTree->Branch("track_z0st",&_track_z0st);
  m_myTree->Branch("track_qOverP",&_track_qOverP);
  m_myTree->Branch("track_qOverPt",&_track_qOverPt);

  m_myTree->Branch("track_d0err",&_track_d0err);
  m_myTree->Branch("track_z0err",&_track_z0err);
  m_myTree->Branch("track_phierr",&_track_phierr);
  m_myTree->Branch("track_thetaerr",&_track_thetaerr);
  m_myTree->Branch("track_z0sterr",&_track_z0sterr);
  m_myTree->Branch("track_qOverPterr",&_track_qOverPterr);
  
  m_myTree->Branch("track_prob",&_track_prob);

  m_myTree->Branch("track_innermostPixLayerHits",&_track_innermostPixLayerHits);
  m_myTree->Branch("track_innermostPixLayerOutliers",&_track_innermostPixLayerOutliers);
  m_myTree->Branch("track_innermostPixLayerSplitHits",&_track_innermostPixLayerSplitHits);
  m_myTree->Branch("track_innermostPixLayerSharedHits",&_track_innermostPixLayerSharedHits);
  m_myTree->Branch("track_innermostExpectPixLayerHits",&_track_innermostExpectPixLayerHits);

  m_myTree->Branch("track_nextToInnermostPixLayerHits",&_track_nextToInnermostPixLayerHits);
  m_myTree->Branch("track_nextToInnermostPixLayerOutliers",&_track_nextToInnermostPixLayerOutliers);
  m_myTree->Branch("track_nextToInnermostPixLayerSplitHits",&_track_nextToInnermostPixLayerSplitHits);
  m_myTree->Branch("track_nextToInnermostPixLayerSharedHits",&_track_nextToInnermostPixLayerSharedHits);
  m_myTree->Branch("track_nextToInnermostExpectPixLayerHits",&_track_nextToInnermostExpectPixLayerHits);

  m_myTree->Branch("track_pixHits",&_track_pixHits);
  m_myTree->Branch("track_pixHoles",&_track_pixHoles);
  m_myTree->Branch("track_pixSharedHits",&_track_pixSharedHits);
  m_myTree->Branch("track_pixOutliers",&_track_pixOutliers);
  m_myTree->Branch("track_pixContribLayers",&_track_pixContribLayers);
  m_myTree->Branch("track_pixSplitHits",&_track_pixSplitHits);
  m_myTree->Branch("track_pixGangedHits",&_track_pixGangedHits);
  m_myTree->Branch("track_pixDead",&_track_pixDead);

  m_myTree->Branch("track_SCTHits",&_track_SCTHits);
  m_myTree->Branch("track_SCTHoles",&_track_SCTHoles);
  m_myTree->Branch("track_SCTDoubleHoles",&_track_SCTDoubleHoles);
  m_myTree->Branch("track_SCTSharedHits",&_track_SCTSharedHits);
  m_myTree->Branch("track_SCTOutliers",&_track_SCTOutliers);  
  m_myTree->Branch("track_SCTDead",&_track_SCTDead);

  m_myTree->Branch("track_TRTHits",&_track_TRTHits);
  m_myTree->Branch("track_TRTHTHits",&_track_TRTHTHits);
  m_myTree->Branch("track_TRTOutliers",&_track_TRTOutliers);
  m_myTree->Branch("track_TRTHTOutliers",&_track_TRTHTOutliers);

  m_myTree->Branch("track_SiHits",&_track_SiHits);
  m_myTree->Branch("track_SiSharedHits",&_track_SiSharedHits);

  m_myTree->Branch("track_fit_chi2",&_track_fit_chi2);
  m_myTree->Branch("track_fit_ndf",&_track_fit_ndf);
  m_myTree->Branch("track_fit_chi2ndf",&_track_fit_chi2ndf);
  m_myTree->Branch("track_fit_chi2prob",&_track_fit_chi2prob);

  m_myTree->Branch("track_cov_matrix",&_track_cov_matrix);
  m_myTree->Branch("track_cov_matrix_det",&_track_cov_matrix_det);

  m_myTree->Branch("track_patternInfo",&_track_patternInfo);

  m_myTree->Branch("track_truth_index",    &_track_truth_index);

  m_myTree->Branch("truth_n",&_truth_n,"truth_n/I");
  m_myTree->Branch("truth_charge",         &_truth_charge);
  m_myTree->Branch("truth_pt",             &_truth_pt);
  m_myTree->Branch("truth_eta",            &_truth_eta);
  m_myTree->Branch("truth_theta",          &_truth_theta);
  m_myTree->Branch("truth_phi",            &_truth_phi);
  m_myTree->Branch("truth_phi0",           &_truth_phi0);
  m_myTree->Branch("truth_mass",           &_truth_mass);
  m_myTree->Branch("truth_d0",             &_truth_d0);
  m_myTree->Branch("truth_z0",             &_truth_z0);
  m_myTree->Branch("truth_qOverP",         &_truth_qOverP);
  m_myTree->Branch("truth_qOverPt",        &_truth_qOverPt);
  m_myTree->Branch("truth_z0st",           &_truth_z0st);
  m_myTree->Branch("truth_prodR",          &_truth_prodR);
  m_myTree->Branch("truth_prodZ",          &_truth_prodZ);
  m_myTree->Branch("truth_barcode",        &_truth_barcode);
  m_myTree->Branch("truth_isCharged",      &_truth_isCharged);
  m_myTree->Branch("truth_status",         &_truth_status);
  m_myTree->Branch("truth_pdgId",          &_truth_pdgId);
  m_myTree->Branch("truth_hasGrandParent", &_truth_hasGrandParent);
  m_myTree->Branch("truth_eleFromGamma",   &_truth_eleFromGamma);
  m_myTree->Branch("truth_hasProdVtx",     &_truth_hasProdVtx);
  m_myTree->Branch("truth_isFromHSProdVtx",&_truth_isFromHSProdVtx);
  m_myTree->Branch("truth_prodVtxRadius",  &_truth_prodVtxRadius);
  m_myTree->Branch("truth_prodVtxZ",  &_truth_prodVtxZ);
  m_myTree->Branch("truth_hasDecayVtx",    &_truth_hasDecayVtx);
  m_myTree->Branch("truth_decayVtxRadius", &_truth_decayVtxRadius);
  m_myTree->Branch("truth_decayVtxZ", &_truth_decayVtxZ);
  m_myTree->Branch("truth_StdSel",         &_truth_StdSel);
  m_myTree->Branch("truth_StdSel_ITk",     &_truth_StdSel_ITk);
  m_myTree->Branch("truth_isFromSUSY",     &_truth_isFromSUSY);
  m_myTree->Branch("truth_SUSY_index",     &_truth_SUSY_index);
  m_myTree->Branch("truth_SUSY_deta",      &_truth_SUSY_deta);
  m_myTree->Branch("truth_SUSY_dphi",      &_truth_SUSY_dphi);
  m_myTree->Branch("truth_PVDV_deta",      &_truth_PVDV_deta);
  m_myTree->Branch("truth_PVDV_dphi",      &_truth_PVDV_dphi);

  m_myTree->Branch("truth_track_index",    &_truth_track_index);

  m_myTree->Branch("truth_parent_index",   &_truth_parent_index);
  m_myTree->Branch("truth_child_index",    &_truth_child_index);
  m_myTree->Branch("truth_n_children",     &_truth_n_children);
  m_myTree->Branch("truth_isFromTau",   &_truth_isFromTau);
  m_myTree->Branch("truth_tau_parent_index",   &_truth_tau_parent_index);
  m_myTree->Branch("truth_isFromB",   &_truth_isFromB);
  m_myTree->Branch("truth_B_parent_index",   &_truth_B_parent_index);

  m_myTree->Branch("gen_vtx_n",&_gen_vtx_n,"gen_vtx_n/I");
  m_myTree->Branch("gen_vtx_x",&_gen_vtx_x,"gen_vtx_x/F");
  m_myTree->Branch("gen_vtx_y",&_gen_vtx_y,"gen_vtx_y/F");
  m_myTree->Branch("gen_vtx_z",&_gen_vtx_z,"gen_vtx_z/F");
  m_myTree->Branch("gen_vtx_delta_z_closest",&_gen_vtx_delta_z_closest,"gen_vtx_delta_z_closest/F");

  m_myTree->Branch("gen_vertices_x",&_gen_vertices_x);
  m_myTree->Branch("gen_vertices_y",&_gen_vertices_y);
  m_myTree->Branch("gen_vertices_z",&_gen_vertices_z);
  m_myTree->Branch("gen_vertices_isHS",&_gen_vertices_isHS);

  m_myTree->Branch("reco_vtx_n",&_reco_vtx_n,"reco_vtx_n/I");
  m_myTree->Branch("reco_vtx_x",&_reco_vtx_x,"reco_vtx_x/F");
  m_myTree->Branch("reco_vtx_y",&_reco_vtx_y,"reco_vtx_y/F");
  m_myTree->Branch("reco_vtx_z",&_reco_vtx_z,"reco_vtx_z/F");
  m_myTree->Branch("reco_vertices_x",&_reco_vertices_x);
  m_myTree->Branch("reco_vertices_y",&_reco_vertices_y);
  m_myTree->Branch("reco_vertices_z",&_reco_vertices_z);

  m_myTree->Branch("mu",&_mu,"mu/F");

  if(m_isHitInfoAvailable){

    m_myTree->Branch("pixCluster_track_index",&_pixCluster_track_index);
    m_myTree->Branch("pixCluster_reco_globX",  &_pixCluster_reco_globX);
    m_myTree->Branch("pixCluster_reco_globY",  &_pixCluster_reco_globY);
    m_myTree->Branch("pixCluster_reco_globZ",  &_pixCluster_reco_globZ);
    m_myTree->Branch("pixCluster_reco_globR",  &_pixCluster_reco_globR);
    m_myTree->Branch("pixCluster_reco_globEta",  &_pixCluster_reco_globEta);
    m_myTree->Branch("pixCluster_reco_globPhi",  &_pixCluster_reco_globPhi);
    m_myTree->Branch("pixCluster_reco_locX",  &_pixCluster_reco_locX);
    m_myTree->Branch("pixCluster_reco_locY",  &_pixCluster_reco_locY);
    m_myTree->Branch("pixCluster_bec", &_pixCluster_bec);
    m_myTree->Branch("pixCluster_isInclined", &_pixCluster_isInclined);
    m_myTree->Branch("pixCluster_layer", &_pixCluster_layer);
    m_myTree->Branch("pixCluster_sizePhi", &_pixCluster_sizePhi);
    m_myTree->Branch("pixCluster_sizeZ", &_pixCluster_sizeZ);
    m_myTree->Branch("pixCluster_localTheta", &_pixCluster_localTheta);
    m_myTree->Branch("pixCluster_omegax", &_pixCluster_omegax);
    m_myTree->Branch("pixCluster_omegay", &_pixCluster_omegay);
    m_myTree->Branch("pixCluster_LorentzShift", &_pixCluster_LorentzShift);
    m_myTree->Branch("pixCluster_etaModule", &_pixCluster_etaModule);
    m_myTree->Branch("pixCluster_phiModule", &_pixCluster_phiModule);
    m_myTree->Branch("pixCluster_etaIndex", &_pixCluster_etaIndex);
    m_myTree->Branch("pixCluster_phiIndex", &_pixCluster_phiIndex);
    m_myTree->Branch("pixCluster_charge", &_pixCluster_charge);
    m_myTree->Branch("pixCluster_ToT", &_pixCluster_ToT);
    m_myTree->Branch("pixCluster_centroid_xphi", &_pixCluster_centroid_xphi);
    m_myTree->Branch("pixCluster_centroid_xeta", &_pixCluster_centroid_xeta);
    m_myTree->Branch("pixCluster_pixelID", &_pixCluster_pixelID);
    m_myTree->Branch("pixCluster_waferID", &_pixCluster_waferID);
    m_myTree->Branch("pixCluster_hitDeco_index", &_pixCluster_hitDeco_index);

    m_myTree->Branch("pixCluster_truth_size", &_pixCluster_truth_size);
    m_myTree->Branch("pixCluster_truth_locX", &_pixCluster_truth_locX);
    m_myTree->Branch("pixCluster_truth_locY", &_pixCluster_truth_locY);
    m_myTree->Branch("pixCluster_truth_barcode",&_pixCluster_truth_barcode);
    m_myTree->Branch("pixCluster_truth_reco_index", &_pixCluster_truth_reco_index);

    m_myTree->Branch("hitDeco_track_index",&_hitDeco_track_index);
    m_myTree->Branch("hitDeco_measurement_region",&_hitDeco_measurement_region);
    m_myTree->Branch("hitDeco_measurement_det",&_hitDeco_measurement_det);
    m_myTree->Branch("hitDeco_measurement_iLayer",&_hitDeco_measurement_iLayer);
    m_myTree->Branch("hitDeco_measurement_type",&_hitDeco_measurement_type);
    m_myTree->Branch("hitDeco_residualLocX",&_hitDeco_residualLocX);
    m_myTree->Branch("hitDeco_residualLocY",&_hitDeco_residualLocY);
    m_myTree->Branch("hitDeco_phiWidth",&_hitDeco_phiWidth);
    m_myTree->Branch("hitDeco_etaWidth",&_hitDeco_etaWidth);
    m_myTree->Branch("hitDeco_measurementLocX",&_hitDeco_measurementLocX);
    m_myTree->Branch("hitDeco_measurementLocY",&_hitDeco_measurementLocY);
    m_myTree->Branch("hitDeco_measurementLocCovX",&_hitDeco_measurementLocCovX);
    m_myTree->Branch("hitDeco_measurementLocCovY",&_hitDeco_measurementLocCovY);
    m_myTree->Branch("hitDeco_trackParamLocX",&_hitDeco_trackParamLocX);
    m_myTree->Branch("hitDeco_trackParamLocY",&_hitDeco_trackParamLocY);
    m_myTree->Branch("hitDeco_angle",&_hitDeco_angle);
    m_myTree->Branch("hitDeco_loceta",&_hitDeco_loceta);
    m_myTree->Branch("hitDeco_pixelID",&_hitDeco_pixelID);

    m_myTree->Branch("stripCluster_track_index",&_stripCluster_track_index);
    m_myTree->Branch("stripCluster_reco_globX",&_stripCluster_reco_globX);
    m_myTree->Branch("stripCluster_reco_globY",&_stripCluster_reco_globY);
    m_myTree->Branch("stripCluster_reco_globZ",&_stripCluster_reco_globZ);
    m_myTree->Branch("stripCluster_reco_globR",&_stripCluster_reco_globR);
    m_myTree->Branch("stripCluster_reco_globPhi",&_stripCluster_reco_globPhi);
    m_myTree->Branch("stripCluster_reco_globEta",&_stripCluster_reco_globEta);
    m_myTree->Branch("stripCluster_reco_locX",&_stripCluster_reco_locX);
    m_myTree->Branch("stripCluster_reco_locY",&_stripCluster_reco_locY);
    m_myTree->Branch("stripCluster_bec",&_stripCluster_bec);
    m_myTree->Branch("stripCluster_layer",&_stripCluster_layer);
    m_myTree->Branch("stripCluster_siWidth",&_stripCluster_siWidth);
    m_myTree->Branch("stripCluster_etaModule",&_stripCluster_etaModule);
    m_myTree->Branch("stripCluster_phiModule",&_stripCluster_phiModule);
    m_myTree->Branch("stripCluster_stripID",&_stripCluster_stripID);
    m_myTree->Branch("stripCluster_side",&_stripCluster_side);
    m_myTree->Branch("stripCluster_strip",&_stripCluster_strip);
    m_myTree->Branch("stripCluster_row",&_stripCluster_row);
    m_myTree->Branch("stripCluster_hitDeco_index", &_stripCluster_hitDeco_index);

    m_myTree->Branch("stripCluster_truth_size", &_stripCluster_truth_size);
    m_myTree->Branch("stripCluster_truth_locX", &_stripCluster_truth_locX);
    m_myTree->Branch("stripCluster_truth_locY", &_stripCluster_truth_locY);
    m_myTree->Branch("stripCluster_truth_barcode",&_stripCluster_truth_barcode);
    m_myTree->Branch("stripCluster_truth_reco_index", &_stripCluster_truth_reco_index);

  }


  return StatusCode::SUCCESS;
}

StatusCode TRKNtupleAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  return StatusCode::SUCCESS;
}

StatusCode TRKNtupleAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed

  clean();

  const xAOD::EventInfo* ei = 0;
  CHECK( evtStore()->retrieve( ei , "EventInfo" ) );
  ATH_MSG_INFO("eventNumber=" << ei->eventNumber() );
  _EventNumber = ei->eventNumber();

  //Truth particles
  SG::ReadHandle<xAOD::TruthParticleContainer> ptruths(m_truthParticleName);
  if (not ptruths.isValid()) {
    return StatusCode::FAILURE;
  }


  SG::ReadHandle<xAOD::TruthEventContainer> truthEventContainer( m_truthEventName);
  const xAOD::TruthEvent* event = (truthEventContainer.isValid()) ? truthEventContainer->at(0) : nullptr;
  const auto& links = event->truthParticleLinks();
  std::vector<const xAOD::TruthParticle*> truthParticlesHS;
  truthParticlesHS.reserve(event->nTruthParticles());
  for (const auto& link : links){
    if(link.isValid()) truthParticlesHS.push_back(*link);
  }



  // Tracks

  IDPVM::CachedGetAssocTruth getAsTruth;

  SG::ReadHandle<xAOD::TrackParticleContainer> ptracks(m_trkParticleName);
  if (not ptracks.isValid()) {
    return StatusCode::FAILURE;
  }

  _track_n = (*ptracks).size();

  _track_cov_matrix.resize(15);

  for (const auto& thisTrack: *ptracks) {

    _track_isInOut.emplace_back(isInsideOut(*thisTrack));
    _track_charge.emplace_back(thisTrack->charge());
    _track_pt.emplace_back(thisTrack->pt()*1e-3);
    _track_eta.emplace_back(thisTrack->eta());
    _track_theta.emplace_back(thisTrack->theta());
    _track_phi.emplace_back(thisTrack->phi());
    _track_phi0.emplace_back(thisTrack->phi0());
    _track_d0.emplace_back(thisTrack->d0());
    _track_z0.emplace_back(thisTrack->z0());
    _track_z0st.emplace_back(thisTrack->z0()*sin(thisTrack->theta()));
    _track_qOverP.emplace_back(thisTrack->qOverP());
    _track_qOverPt.emplace_back(thisTrack->qOverP()*1e3/sin(thisTrack->theta()));

    _track_d0err.emplace_back( thisTrack->isAvailable<float>("d0err") ? thisTrack->auxdata<float>("d0err") : -1.);
    _track_z0err.emplace_back( thisTrack->isAvailable<float>("z0err") ? thisTrack->auxdata<float>("z0err") : -1.);
    _track_phierr.emplace_back( thisTrack->isAvailable<float>("phierr") ? thisTrack->auxdata<float>("phierr") : -1.);    
    _track_thetaerr.emplace_back( thisTrack->isAvailable<float>("thetaerr") ? thisTrack->auxdata<float>("thetaerr") : -1.);
    
    bool z0sterr_isAvailable = thisTrack->isAvailable<float>("z0err") && thisTrack->isAvailable<float>("thetaerr");
    float z0sterr = -1.;
    if(z0sterr_isAvailable){
      float term1 = thisTrack->auxdata<float>("z0err")*sin(thisTrack->theta());
      float term2 = thisTrack->z0()*thisTrack->auxdata<float>("thetaerr")*cos(thisTrack->theta());
      z0sterr = sqrt(term1*term1 + term2*term2);
    }
    _track_z0sterr.emplace_back( z0sterr );
    _track_qOverPterr.emplace_back( thisTrack->isAvailable<float>("qopterr") ? thisTrack->auxdata<float>("qopterr") : -1.);
   
    _track_prob.emplace_back( thisTrack->isAvailable<float>("truthMatchProbability") ? thisTrack->auxdata<float>("truthMatchProbability") : -1.);
    

    //IBL
    uint8_t iIBLHits(0), iIBLOutliers(0), iIBLSplitHits(0), iIBLSharedHits(0), iIBLExpHits(0);
    int innermostPixLayerHits(-1), innermostPixLayerOutliers(-1), innermostPixLayerSplitHits(-1), innermostPixLayerSharedHits(-1), innermostExpectPixLayerHits;

    if(thisTrack->summaryValue(iIBLHits, xAOD::numberOfInnermostPixelLayerHits)){
      innermostPixLayerHits = iIBLHits;
    }
    if(thisTrack->summaryValue(iIBLOutliers, xAOD::numberOfInnermostPixelLayerOutliers)){
      innermostPixLayerOutliers = iIBLOutliers;
    }
    if(thisTrack->summaryValue(iIBLSplitHits, xAOD::numberOfInnermostPixelLayerSplitHits)){
      innermostPixLayerSplitHits = iIBLSplitHits;
    }
    if(thisTrack->summaryValue(iIBLSharedHits, xAOD::numberOfInnermostPixelLayerSharedHits)){
      innermostPixLayerSharedHits = iIBLSharedHits;
    }
    if(thisTrack->summaryValue(iIBLExpHits, xAOD::expectInnermostPixelLayerHit)){
      innermostExpectPixLayerHits = iIBLExpHits;
    }
    
    _track_innermostPixLayerHits.emplace_back(innermostPixLayerHits);
    _track_innermostPixLayerOutliers.emplace_back(innermostPixLayerOutliers);
    _track_innermostPixLayerSplitHits.emplace_back(innermostPixLayerSplitHits);
    _track_innermostPixLayerSharedHits.emplace_back(innermostPixLayerSharedHits);
    _track_innermostExpectPixLayerHits.emplace_back(innermostExpectPixLayerHits);


    //B-layer
    uint8_t iBLayerHits(0), iBLayerOutliers(0), iBLayerSplitHits(0), iBLayerSharedHits(0), iBLayerExpHits(0);
    int nextToInnermostPixLayerHits(-1), nextToInnermostPixLayerOutliers(-1), nextToInnermostPixLayerSplitHits(-1), nextToInnermostPixLayerSharedHits(-1), nextToInnermostExpectPixLayerHits;

    if(thisTrack->summaryValue(iBLayerHits, xAOD::numberOfNextToInnermostPixelLayerHits)){
      nextToInnermostPixLayerHits = iBLayerHits;
    }
    if(thisTrack->summaryValue(iBLayerOutliers, xAOD::numberOfNextToInnermostPixelLayerOutliers)){
      nextToInnermostPixLayerOutliers = iBLayerOutliers;
    }
    if(thisTrack->summaryValue(iBLayerSplitHits, xAOD::numberOfNextToInnermostPixelLayerSplitHits)){
      nextToInnermostPixLayerSplitHits = iBLayerSplitHits;
    }
    if(thisTrack->summaryValue(iBLayerSharedHits, xAOD::numberOfNextToInnermostPixelLayerSharedHits)){
      nextToInnermostPixLayerSharedHits = iBLayerSharedHits;
    }
    if(thisTrack->summaryValue(iBLayerExpHits, xAOD::expectNextToInnermostPixelLayerHit)){
      nextToInnermostExpectPixLayerHits = iBLayerExpHits;
    }

    _track_nextToInnermostPixLayerHits.emplace_back(nextToInnermostPixLayerHits);
    _track_nextToInnermostPixLayerOutliers.emplace_back(nextToInnermostPixLayerOutliers);
    _track_nextToInnermostPixLayerSplitHits.emplace_back(nextToInnermostPixLayerSplitHits);
    _track_nextToInnermostPixLayerSharedHits.emplace_back(nextToInnermostPixLayerSharedHits);
    _track_nextToInnermostExpectPixLayerHits.emplace_back(nextToInnermostExpectPixLayerHits);

    //Pixels
    uint8_t iPixHits(0), iPixHoles(0), iPixSharedHits(0), iPixOutliers(0), iPixContribLayers(0), iPixSplitHits(0), iPixGangedHits(0), iPixDead(0);
    int pixHits(-1), pixHoles(-1), pixSharedHits(-1), pixOutliers(-1), pixContribLayers(-1), pixSplitHits(-1), pixGangedHits(-1), pixDead(-1);

    if (thisTrack->summaryValue(iPixHits, xAOD::numberOfPixelHits)) {
      pixHits = iPixHits;
    }
    if (thisTrack->summaryValue(iPixHoles, xAOD::numberOfPixelHoles)) {
      pixHoles = iPixHoles;
    }
    if (thisTrack->summaryValue(iPixSharedHits, xAOD::numberOfPixelSharedHits)) {
      pixSharedHits = iPixSharedHits;
    }
    if (thisTrack->summaryValue(iPixOutliers, xAOD::numberOfPixelOutliers)) {
      pixOutliers = iPixOutliers;
    }
    if (thisTrack->summaryValue(iPixContribLayers, xAOD::numberOfContribPixelLayers)) {
      pixContribLayers = iPixContribLayers;
    }
    if (thisTrack->summaryValue(iPixSplitHits, xAOD::numberOfPixelSplitHits)) {
      pixSplitHits = iPixSplitHits;
    }
    if (thisTrack->summaryValue(iPixGangedHits, xAOD::numberOfGangedPixels)) {
      pixGangedHits = iPixGangedHits;
    }
    if (thisTrack->summaryValue(iPixDead, xAOD::numberOfPixelDeadSensors)) {
      pixDead = iPixDead;
    }

    _track_pixHits.emplace_back(pixHits);
    _track_pixHoles.emplace_back(pixHoles);
    _track_pixSharedHits.emplace_back(pixSharedHits);
    _track_pixOutliers.emplace_back(pixOutliers);
    _track_pixContribLayers.emplace_back(pixContribLayers);
    _track_pixSplitHits.emplace_back(pixSplitHits);
    _track_pixGangedHits.emplace_back(pixGangedHits);
    _track_pixDead.emplace_back(pixDead);

    //SCT
    uint8_t iSCTHits(0), iSCTHoles(0), iSCTDoubleHoles(0), iSCTSharedHits(0), iSCTOutliers(0), iSCTDead(0);
    int SCTHits(-1), SCTHoles(-1), SCTDoubleHoles(-1), SCTSharedHits(-1), SCTOutliers(-1), SCTDead(-1);

    if (thisTrack->summaryValue(iSCTHits, xAOD::numberOfSCTHits)) {
      SCTHits = iSCTHits;
    }
    if (thisTrack->summaryValue(iSCTHoles, xAOD::numberOfSCTHoles)) {
      SCTHoles = iSCTHoles;
    }
    if (thisTrack->summaryValue(iSCTDoubleHoles, xAOD::numberOfSCTDoubleHoles)) {
      SCTDoubleHoles = iSCTDoubleHoles;
    }
    if (thisTrack->summaryValue(iSCTSharedHits, xAOD::numberOfSCTSharedHits)) {
      SCTSharedHits = iSCTSharedHits;
    }
    if (thisTrack->summaryValue(iSCTOutliers, xAOD::numberOfSCTOutliers)) {
      SCTOutliers = iSCTOutliers;
    }    
    if (thisTrack->summaryValue(iSCTDead, xAOD::numberOfSCTDeadSensors)) {
      SCTDead = iSCTDead;
    }

    _track_SCTHits.emplace_back(SCTHits);
    _track_SCTHoles.emplace_back(SCTHoles);
    _track_SCTDoubleHoles.emplace_back(SCTDoubleHoles);
    _track_SCTSharedHits.emplace_back(SCTSharedHits);
    _track_SCTOutliers.emplace_back(SCTOutliers);
    _track_SCTDead.emplace_back(SCTDead);


    //TRT
    uint8_t iTRTHits(0), iTRTHTHits(0), iTRTOutliers(0), iTRTHTOutliers(0);
    int TRTHits(-1), TRTHTHits(-1), TRTOutliers(-1), TRTHTOutliers(-1);
    
    if (thisTrack->summaryValue(iTRTHits, xAOD::numberOfTRTHits)) {
      TRTHits = iTRTHits;
    }
    if (thisTrack->summaryValue(iTRTHTHits, xAOD::numberOfTRTHighThresholdOutliers)) {
      TRTHTHits = iTRTHTHits;
    }
    if (thisTrack->summaryValue(iTRTOutliers, xAOD::numberOfTRTOutliers)) {
      TRTOutliers = iTRTOutliers;
    }
    if (thisTrack->summaryValue(iTRTHTOutliers, xAOD::numberOfTRTHighThresholdOutliers)) {
      TRTHTOutliers = iTRTHTOutliers;
    }
    
    _track_TRTHits.emplace_back(TRTHits);
    _track_TRTHTHits.emplace_back(TRTHTHits);
    _track_TRTOutliers.emplace_back(TRTOutliers);
    _track_TRTHTOutliers.emplace_back(TRTHTOutliers);

    _track_SiHits.emplace_back(pixHits + SCTHits);
    _track_SiSharedHits.emplace_back(pixSharedHits + SCTSharedHits);

    bool loose = abs(thisTrack->eta())<2.5
      && (pixHits + SCTHits + pixDead + SCTDead)>=7
      && (pixSharedHits + 0.5*SCTSharedHits)<=1
      && (pixHoles + SCTHoles)<=2
      && pixHoles<=1;

    _track_isLoose.emplace_back(loose);
    
    bool tightPrimary = abs(thisTrack->eta())<2.5
      && (pixHits + SCTHits + pixDead + SCTDead)>=9
      && (pixSharedHits + 0.5*SCTSharedHits)<=1
      && (pixHoles + SCTHoles)<=2
      && pixHoles<=0
      && (abs(thisTrack->eta())<1.65 || (pixHits + SCTHits + pixDead + SCTDead)>=11)
      && (max(innermostPixLayerHits,(int)!innermostExpectPixLayerHits) + max(nextToInnermostPixLayerHits,(int)!nextToInnermostExpectPixLayerHits))>=1;

    _track_isTightPrimary.emplace_back(tightPrimary);

    float chi2 = thisTrack->chiSquared();
    int ndf = thisTrack->numberDoF();

    _track_fit_chi2.emplace_back(chi2);
    _track_fit_ndf.emplace_back(ndf);
    _track_fit_chi2ndf.emplace_back(chi2/ndf);
    _track_fit_chi2prob.emplace_back(TMath::Prob(chi2,ndf));

    xAOD::ParametersCovMatrix_t covMatrix = thisTrack->definingParametersCovMatrix();
    for(unsigned int i=0;i<5;i++){
      for(unsigned int j=0;j<=i;j++){
	int index = int(0.5*i*(i+1)+j);
	_track_cov_matrix[index].emplace_back(covMatrix(i,j));
      }
    }
    _track_cov_matrix_det.push_back(covMatrix.determinant());

    _track_patternInfo.push_back(long(thisTrack->patternRecoInfo().to_ulong()));

    //Truth
    int truth_index = -1;

    const xAOD::TruthParticle* associatedTruth = getAsTruth.getTruth(thisTrack);
    
    int itruth = -1;
    for (const auto& thisTruth: *ptruths) {
      itruth++;
      if(thisTruth == associatedTruth){
	truth_index = itruth;
	break;
      }
    }
    
    _track_truth_index.push_back(truth_index);


  }

  
  //Truth particles

  _truth_n = (*ptruths).size();

  for (const auto& thisTruth: *ptruths) {

    _truth_charge.emplace_back(thisTruth->charge());
    _truth_pt.emplace_back(thisTruth->pt()*1e-3);
    _truth_eta.emplace_back(thisTruth->eta());
    _truth_theta.emplace_back(thisTruth->isAvailable<float>("theta") ? thisTruth->auxdata<float>("theta") : -1e9);
    _truth_phi.emplace_back(thisTruth->phi());
    _truth_phi0.emplace_back(thisTruth->isAvailable<float>("phi") ? thisTruth->auxdata<float>("phi") : -1e9);
    _truth_mass.emplace_back(thisTruth->m());
    _truth_d0.emplace_back(thisTruth->isAvailable<float>("d0") ? thisTruth->auxdata<float>("d0") : -1e9);
    _truth_z0.emplace_back(thisTruth->isAvailable<float>("z0") ? thisTruth->auxdata<float>("z0") : -1e9);
    _truth_qOverP.emplace_back(thisTruth->isAvailable<float>("qOverP") ? thisTruth->auxdata<float>("qOverP") : -1e9);
    bool isQOverPtAvailable = thisTruth->isAvailable<float>("qOverP") && thisTruth->isAvailable<float>("theta");
    _truth_qOverPt.emplace_back(isQOverPtAvailable ? thisTruth->auxdata<float>("qOverP")*1e3/sin(thisTruth->auxdata<float>("theta")) : -1e9);
    _truth_barcode.emplace_back(thisTruth->barcode());
    bool isCharged = !(thisTruth->isNeutral());
    _truth_isCharged.emplace_back(isCharged);
    _truth_status.emplace_back(thisTruth->status());
    _truth_pdgId.emplace_back(thisTruth->pdgId());
    bool hasGrandParent = !( (thisTruth->nParents()==0) 
			     || ((thisTruth->nParents()==1) && (thisTruth->parent(0)) && (thisTruth->parent(0)->nParents()==0)) );
    _truth_hasGrandParent.emplace_back(hasGrandParent);
    bool isEleFromGamma = thisTruth->absPdgId()==11 
      && (thisTruth->nParents()>=1) 
      && (thisTruth->parent(0))
      && (thisTruth->parent(0)->pdgId()==22);
    _truth_eleFromGamma.emplace_back(isEleFromGamma);
    bool hasProdVtx = thisTruth->hasProdVtx();
    _truth_hasProdVtx.emplace_back(hasProdVtx);
    _truth_prodVtxRadius.emplace_back(hasProdVtx ? thisTruth->prodVtx()->perp() : -1e9);
    _truth_prodVtxZ.emplace_back(hasProdVtx ? thisTruth->prodVtx()->z() : -1e9);
    bool hasDecayVtx = thisTruth->hasDecayVtx();
    _truth_hasDecayVtx.emplace_back(hasDecayVtx);
    _truth_decayVtxRadius.emplace_back(hasDecayVtx ? thisTruth->decayVtx()->perp() : -1e9);
    _truth_decayVtxZ.emplace_back(hasDecayVtx ? thisTruth->decayVtx()->z() : -1e9);
    bool StdSel_truth = thisTruth->pt()*1e-3>0.4 && abs(thisTruth->eta())<2.5 
      && thisTruth->barcode()<200e3 && isCharged && thisTruth->status()==1
      && hasProdVtx && thisTruth->prodVtx()->perp()<110.;
    _truth_StdSel.emplace_back(StdSel_truth);
    bool StdSel_truth_ITk = thisTruth->pt()*1e-3>1. && abs(thisTruth->eta())<4.0
      && thisTruth->barcode()<200e3 && isCharged && thisTruth->status()==1
      && hasProdVtx && thisTruth->prodVtx()->perp()<260.;
    _truth_StdSel_ITk.emplace_back(StdSel_truth_ITk);
    _truth_z0st.emplace_back(thisTruth->isAvailable<float>("z0st") ? thisTruth->auxdata<float>("z0st") : -1e9);
    _truth_prodR.emplace_back(thisTruth->isAvailable<float>("prodR") ? thisTruth->auxdata<float>("prodR") : -1e9);
    _truth_prodZ.emplace_back(thisTruth->isAvailable<float>("prodZ") ? thisTruth->auxdata<float>("prodZ") : -1e9);

    bool isFromHSProdVtx = false;
    for (const auto& thisTruthHS: truthParticlesHS) {
      if(thisTruth == thisTruthHS){
	isFromHSProdVtx = true;
	break;
      }
    }
    _truth_isFromHSProdVtx.push_back(isFromHSProdVtx);

    //Track matching

    int track_index = -1;
    float bestMatchProb = -1.;

    int itrack = -1;
    for (const auto& thisTrack: *ptracks) {
      itrack++;
      const xAOD::TruthParticle* associatedTruth = getAsTruth.getTruth(thisTrack);
      
      if(thisTruth == associatedTruth){
	float prob = _track_prob[itrack];
	if(prob>bestMatchProb){
	  bestMatchProb = prob;
	  track_index = itrack;
	}
      }

    }

    _truth_track_index.push_back(track_index);

    bool found_parent = false;
    int parent_index = -1;
    if(thisTruth->nParents()>0){
      const xAOD::TruthParticle* parent = thisTruth->parent(0);
      for (const auto& thisTruth2: *ptruths){
	parent_index++;
	if(thisTruth2 == parent){
	  found_parent = true;
	  break;
	}
      }
      if(!found_parent) parent_index = -1;
    }

    _truth_parent_index.push_back(parent_index);

    bool found_SUSY = false;
    int SUSY_index = -1;
    const xAOD::TruthParticle* SUSYCandTruth = thisTruth;
    while(SUSYCandTruth->nParents()>0){
      const xAOD::TruthParticle* parent = SUSYCandTruth->parent(0);
      if(!parent) break;
      SUSYCandTruth = parent;
      if(abs(SUSYCandTruth->pdgId())>1000000){
	found_SUSY = true;
	for (const auto& thisTruth2: *ptruths){
	  SUSY_index++;
	  if(thisTruth2 == SUSYCandTruth) break;
	}
	break;
      }
    }

    _truth_isFromSUSY.push_back(found_SUSY);
    _truth_SUSY_index.push_back(SUSY_index);

    TLorentzVector thisTruth_tlv;
    thisTruth_tlv.SetPtEtaPhiM(thisTruth->pt(),thisTruth->eta(),thisTruth->phi(),thisTruth->m());

    float SUSY_deta = -9999.;
    float SUSY_dphi = -9999.;
    if(found_SUSY){
      TLorentzVector SUSY_tlv;
      SUSY_tlv.SetPtEtaPhiM(SUSYCandTruth->pt(),SUSYCandTruth->eta(),SUSYCandTruth->phi(),SUSYCandTruth->m());
      SUSY_deta = thisTruth_tlv.Eta()-SUSY_tlv.Eta();
      SUSY_dphi = thisTruth_tlv.DeltaPhi(SUSY_tlv);
    }

    _truth_SUSY_deta.push_back(SUSY_deta);
    _truth_SUSY_dphi.push_back(SUSY_dphi);

    vector<int> child_indices;
    for (unsigned i_ch=0; i_ch<thisTruth->nChildren(); i_ch++){
      const xAOD::TruthParticle* child = thisTruth->child(i_ch);

      bool found_child = false;
      int child_index = -1;
      for (const auto& thisTruth2: *ptruths){
	child_index++;
	if(thisTruth2 == child){
	  found_child = true;
	  break;
	}
      }
      if(!found_child) child_index = -1;
      child_indices.push_back(child_index);
    }

    _truth_child_index.push_back(child_indices);
    _truth_n_children.push_back(child_indices.size());

  }


  //Truth vertex

  for (const xAOD::TruthEvent* evt : *truthEventContainer) {

    const xAOD::TruthVertex* vtx = evt->signalProcessVertex();

    if(vtx==0) continue;

    if(_gen_vertices_x.size()==0){
      _gen_vtx_x = vtx->x();
      _gen_vtx_y = vtx->y();
      _gen_vtx_z = vtx->z();
    }

    _gen_vertices_x.push_back(vtx->x());
    _gen_vertices_y.push_back(vtx->y());
    _gen_vertices_z.push_back(vtx->z());
    _gen_vertices_isHS.push_back(true);

  }



  //Will use parent indices so need for them to be filled properly with first loop

  int truth = -1;
  for (const auto& thisTruth: *ptruths) {

    truth++;

    bool isFromTau = false;
    int tau_parent_index = -1;
    if(thisTruth->nParents()>0){
      const xAOD::TruthParticle* parent = thisTruth->parent(0);
      if(parent!=0 && abs(parent->pdgId())==15){
	isFromTau = true;
	tau_parent_index = _truth_parent_index[truth];
      }
      else if(abs(thisTruth->pdgId())==22 && parent!=0 && abs(parent->pdgId())==111 && parent->nParents()>0){
	const xAOD::TruthParticle* grandparent = parent->parent(0);
	if(abs(grandparent->pdgId())==15){
	  isFromTau = true;
	  int parent_index = _truth_parent_index[truth];
	  tau_parent_index = _truth_parent_index[parent_index];
	}
      }
    }

    _truth_isFromTau.push_back(isFromTau);
    _truth_tau_parent_index.push_back(tau_parent_index);


    bool isFromB = false;
    int B_parent_index = truth;

    while(B_parent_index>=0 && !isFromB){
      B_parent_index = _truth_parent_index[B_parent_index];
      if(B_parent_index>=0 && ( (500<abs(_truth_pdgId[B_parent_index]) && abs(_truth_pdgId[B_parent_index])<600) || (5000<abs(_truth_pdgId[B_parent_index]) && abs(_truth_pdgId[B_parent_index])<6000) ) ) isFromB = true;
    }
    if(!isFromB) B_parent_index = -1;

    _truth_isFromB.push_back(isFromB);
    _truth_B_parent_index.push_back(B_parent_index);


    TLorentzVector thisTruth_tlv;
    thisTruth_tlv.SetPtEtaPhiM(thisTruth->pt(),thisTruth->eta(),thisTruth->phi(),thisTruth->m());

    float PVDV_deta = -9999.;
    float PVDV_dphi = -9999.;
    if(thisTruth->hasProdVtx()){
      TLorentzVector PVDV_tlv;
      PVDV_tlv.SetXYZM(thisTruth->prodVtx()->x()-_gen_vtx_x,
		       thisTruth->prodVtx()->y()-_gen_vtx_y,
		       thisTruth->prodVtx()->z()-_gen_vtx_z,
		       0.);
      PVDV_deta = thisTruth_tlv.Eta()-PVDV_tlv.Eta();
      PVDV_dphi = thisTruth_tlv.DeltaPhi(PVDV_tlv);
    }
    _truth_PVDV_deta.push_back(PVDV_deta);
    _truth_PVDV_dphi.push_back(PVDV_dphi);

  }





  SG::ReadHandle<xAOD::TruthPileupEventContainer> pileupEventContainer( m_pileupEventName);

  if(pileupEventContainer.isValid()){

    for (const xAOD::TruthPileupEvent* evt : *pileupEventContainer){

      auto links = evt->truthVertexLinks();
      if (links.size() == 0){
	continue;
      }
      auto link = links.at(0);

      if (link.isValid()){
	auto vtx = *link;

	_gen_vertices_x.push_back(vtx->x());
	_gen_vertices_y.push_back(vtx->y());
	_gen_vertices_z.push_back(vtx->z());
	_gen_vertices_isHS.push_back(false);

	float delta_z = _gen_vtx_z - vtx->z();

	if(_gen_vtx_delta_z_closest<-0.9e9 || abs(delta_z)<abs(_gen_vtx_delta_z_closest)) _gen_vtx_delta_z_closest = delta_z;

      }

    }

  _gen_vtx_n = _gen_vertices_x.size();


  }


  //Reco vertex

  SG::ReadHandle<xAOD::VertexContainer>  pvertex(m_vertexContainerName);
  const xAOD::Vertex* pvtx = nullptr;

  if (pvertex.isValid() and not pvertex->empty()) {
    ATH_MSG_DEBUG("Number of vertices retrieved for this event " << pvertex->size());
    const auto& stdVertexContainer = pvertex->stdcont();

    _reco_vtx_n = 0;
    for(const auto& vtx : stdVertexContainer){
      if(!acceptVertex(vtx)) continue;
      _reco_vtx_n++;
      _reco_vertices_x.push_back(vtx->x());
      _reco_vertices_y.push_back(vtx->y());
      _reco_vertices_z.push_back(vtx->z());
    }

    // find first non-dummy vertex
    auto findVtx = std::find_if(stdVertexContainer.begin(), stdVertexContainer.end(), acceptVertex);
    pvtx = (findVtx == stdVertexContainer.end()) ? nullptr : *findVtx;
    _reco_vtx_x = pvtx!=0 ? pvtx->x() : -9999.;
    _reco_vtx_y = pvtx!=0 ? pvtx->y() : -9999.;
    _reco_vtx_z = pvtx!=0 ? pvtx->z() : -9999.;

  } 
  else {
    ATH_MSG_WARNING("Skipping vertexing");
  }


  _mu = ei->actualInteractionsPerCrossing();



  // Cluster information only available in ESD or DAOD_IDTRKVALID

  typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > > MeasurementsOnTrack;

  if(m_isHitInfoAvailable){

    int itrack = -1;
    for (const auto& thisTrack: *ptracks) {
      itrack++;

      // Get the MSOS's
      if( ! thisTrack->isAvailable< MeasurementsOnTrack >( "msosLink" ) ) continue;
      const MeasurementsOnTrack& measurementsOnTrack = thisTrack->auxdata< MeasurementsOnTrack >( "msosLink" );

      for( auto msos_iter = measurementsOnTrack.begin(); msos_iter != measurementsOnTrack.end(); ++msos_iter) {
	//Check if the element link is valid

	if( ! (*msos_iter).isValid() )  continue;

	const xAOD::TrackStateValidation* msos = *(*msos_iter);

	if( msos->type() != Trk::TrackStateOnSurface::Measurement )  //its a measurement on track
	  continue;

	//Get cluster
	if(  msos->trackMeasurementValidationLink().isValid() && *(msos->trackMeasurementValidationLink()) ){

	  const xAOD::TrackMeasurementValidation* cluster =  *(msos->trackMeasurementValidationLink());
	  // Access a dressed member
	  std::vector<float> sihit_startPosX, sihit_startPosY, sihit_startPosZ, sihit_endPosX, sihit_endPosY, sihit_endPosZ;
	  std::vector<int>   sihit_barcode;
	  std::vector<int>   truth_barcode;
	  std::vector<int>   sihit_match;

	  float localTheta, omegax, omegay, LorentzShift, centroid_xphi, centroid_xeta, charge;
	  int   barrel_endcap, isInclined, layer_disk, sizeZ, sizePhi, etaModule, phiModule, etaIndex, phiIndex, ToT, waferID, siWidth, side, strip, row;
	  uint64_t detElementID;

	  if( cluster->isAvailable< std::vector<int> >( "truth_barcode" ) )
	    truth_barcode = cluster->auxdata< std::vector<int> >("truth_barcode");

	  if( cluster->isAvailable< std::vector<int> >( "sihit_barcode" ))
	    sihit_barcode = cluster->auxdata< std::vector<int> >( "sihit_barcode" );

	  if( cluster->isAvailable< std::vector<int> >( "sihit_match" ))
	    sihit_match = cluster->auxdata< std::vector<int> >( "sihit_match" );

	  if( cluster->isAvailable< std::vector<float> >( "sihit_startPosX" ))
	    sihit_startPosX = cluster->auxdata< std::vector<float> >( "sihit_startPosX" );

	  if( cluster->isAvailable< std::vector<float> >( "sihit_startPosY" ))
	    sihit_startPosY = cluster->auxdata< std::vector<float> >( "sihit_startPosY" );

	  if( cluster->isAvailable< std::vector<float> >( "sihit_startPosZ" ))
	    sihit_startPosZ = cluster->auxdata< std::vector<float> >( "sihit_startPosZ" );

	  if( cluster->isAvailable< std::vector<float> >( "sihit_endPosX" ))
	    sihit_endPosX = cluster->auxdata< std::vector<float> >( "sihit_endPosX" );

	  if( cluster->isAvailable< std::vector<float> >( "sihit_endPosY" ))
	    sihit_endPosY = cluster->auxdata< std::vector<float> >( "sihit_endPosY" );

	  if( cluster->isAvailable< std::vector<float> >( "sihit_endPosZ" ))
	    sihit_endPosZ = cluster->auxdata< std::vector<float> >( "sihit_endPosZ" );

	  if (cluster->isAvailable< uint64_t > ("detectorElementID" ) )
	    detElementID = cluster->auxdata< uint64_t >( "detectorElementID" );

	  if (cluster->isAvailable< int > ("waferID" ) )
	    waferID = cluster->auxdata< int >( "waferID" );

	  if (cluster->isAvailable< int > ("bec"))
	    barrel_endcap = cluster->auxdata< int >( "bec" );

	  if (cluster->isAvailable< int > ("isInclined"))
	    isInclined = cluster->auxdata< int >( "isInclined" );

	  if (cluster->isAvailable< int > ("layer"))
	    layer_disk = cluster->auxdata< int >( "layer" );

	  if (cluster->isAvailable< int > ("sizePhi"))
	    sizePhi = cluster->auxdata< int >( "sizePhi" );

	  if (cluster->isAvailable< int > ("sizeZ"))
	    sizeZ = cluster->auxdata< int >( "sizeZ" );

	  if (cluster->isAvailable< int > ("eta_module"))
	    etaModule = cluster->auxdata< int >( "eta_module" );

	  if (cluster->isAvailable< int > ("phi_module"))
	    phiModule = cluster->auxdata< int >( "phi_module" );

    if (cluster->isAvailable< int > ("eta_pixel_index"))
	    etaIndex = cluster->auxdata< int >( "eta_pixel_index" );

    if (cluster->isAvailable< int > ("phi_pixel_index"))
	    phiIndex = cluster->auxdata< int >( "phi_pixel_index" );

    if (cluster->isAvailable< float > ("charge"))
	    charge = cluster->auxdata< float >( "charge" );

    if (cluster->isAvailable< int > ("ToT"))
	    ToT = cluster->auxdata< int >( "ToT" );

	  if (cluster->isAvailable< float > ("omegax"))
	    omegax = cluster->auxdata< float >( "omegax" );

	  if (cluster->isAvailable< float > ("omegay"))
	    omegay = cluster->auxdata< float >( "omegay" );

	  if (cluster->isAvailable< float > ("LorentzCorrection"))
	    LorentzShift = cluster->auxdata< float >( "LorentzCorrection" );

	  if (cluster->isAvailable< float > ("centroid_xphi"))
	    centroid_xphi = cluster->auxdata< float >( "centroid_xphi" );

	  if (cluster->isAvailable< float > ("centroid_xeta"))
	    centroid_xeta = cluster->auxdata< float >( "centroid_xeta" );

	  if (msos->isAvailable< float > ("localTheta"))
	    localTheta = msos->auxdata< float >( "localTheta" );

	  if (cluster->isAvailable< int > ("siWidth"))
	    siWidth = cluster->auxdata< int >( "siWidth" );

	  if (cluster->isAvailable< int > ("side"))
	    side = cluster->auxdata< int >( "side" );

	  if (cluster->isAvailable< int > ("strip"))
	    strip = cluster->auxdata< int >( "strip" );

	  if (cluster->isAvailable< int > ("row"))
	    row = cluster->auxdata< int >( "row" );

	  float reco_globX = cluster->globalX();
	  float reco_globY = cluster->globalY();
	  float reco_globZ = cluster->globalZ();
	  float reco_globR = sqrt(pow(reco_globX,2)+pow(reco_globY,2));
	  TVector3 reco_glob(reco_globX,reco_globY,reco_globZ);
	  float reco_globEta = reco_glob.Eta();
	  float reco_globPhi = reco_glob.Phi();

	  float reco_locX  = cluster->localX();
	  float reco_locY  = cluster->localY();

	  if( msos->detType() == Trk::TrackState::Pixel ){

	    _pixCluster_track_index.push_back(itrack);

	    _pixCluster_reco_globX.push_back(reco_globX);
	    _pixCluster_reco_globY.push_back(reco_globY);
	    _pixCluster_reco_globZ.push_back(reco_globZ);
	    _pixCluster_reco_globR.push_back(reco_globR);
	    _pixCluster_reco_globEta.push_back(reco_globEta);
	    _pixCluster_reco_globPhi.push_back(reco_globPhi);

	    _pixCluster_reco_locX.push_back(reco_locX);
	    _pixCluster_reco_locY.push_back(reco_locY);

	    _pixCluster_bec.push_back(barrel_endcap);
	    _pixCluster_isInclined.push_back(isInclined);
	    _pixCluster_layer.push_back(layer_disk);
	    _pixCluster_sizePhi.push_back(sizePhi);
	    _pixCluster_sizeZ.push_back(sizeZ);
	    _pixCluster_localTheta.push_back(localTheta);
	    _pixCluster_omegax.push_back(omegax);
	    _pixCluster_omegay.push_back(omegay);
	    _pixCluster_LorentzShift.push_back(LorentzShift);
	    _pixCluster_etaModule.push_back(etaModule);
	    _pixCluster_phiModule.push_back(phiModule);
      _pixCluster_etaIndex.push_back(etaIndex);
      _pixCluster_phiIndex.push_back(phiIndex);
      _pixCluster_charge.push_back(charge);
      _pixCluster_ToT.push_back(ToT);
	    _pixCluster_centroid_xeta.push_back(centroid_xeta);
	    _pixCluster_centroid_xphi.push_back(centroid_xphi);
	    _pixCluster_pixelID.push_back(detElementID);
	    _pixCluster_waferID.push_back(waferID);

	    _pixCluster_truth_size.push_back(truth_barcode.size());

	  }

	  else if( msos->detType() == Trk::TrackState::SCT ){

	    _stripCluster_track_index.push_back(itrack);

	    _stripCluster_reco_globX.push_back(reco_globX);
	    _stripCluster_reco_globY.push_back(reco_globY);
	    _stripCluster_reco_globZ.push_back(reco_globZ);
	    _stripCluster_reco_globR.push_back(reco_globR);
	    _stripCluster_reco_globEta.push_back(reco_globEta);
	    _stripCluster_reco_globPhi.push_back(reco_globPhi);

	    _stripCluster_reco_locX.push_back(reco_locX);
	    _stripCluster_reco_locY.push_back(reco_locY);

	    _stripCluster_bec.push_back(barrel_endcap);
	    _stripCluster_layer.push_back(layer_disk);
	    _stripCluster_siWidth.push_back(siWidth);
	    _stripCluster_etaModule.push_back(etaModule);
	    _stripCluster_phiModule.push_back(phiModule);
	    _stripCluster_stripID.push_back(detElementID);
	    _stripCluster_side.push_back(side);
	    _stripCluster_strip.push_back(strip);
	    _stripCluster_row.push_back(row);

	    _stripCluster_truth_size.push_back(truth_barcode.size());

	  }

	  for( auto barcode = truth_barcode.begin(); barcode !=truth_barcode.end(); barcode++) {

	    auto it = std::find(sihit_barcode.begin(),sihit_barcode.end(),*barcode);
	    int pos = std::distance(sihit_barcode.begin(), it);

	    float truth_locX = -999.;
	    float truth_locY = -999.;

	    if(sihit_barcode.size()>0 && it!=sihit_barcode.end()
	       && sihit_startPosZ.at(pos)*sihit_endPosZ.at(pos) < 0) { // check if the truth barcode is found in the sihit_barcode

	      truth_locX = 0.5*(sihit_startPosX.at(pos)+sihit_endPosX.at(pos));
	      truth_locY = 0.5*(sihit_startPosY.at(pos)+sihit_endPosY.at(pos));

	      //correct position
	      if ( fabs(sihit_startPosZ.at(pos)+sihit_endPosZ.at(pos)) > 1e-6 ) {

		double z = std::max(fabs(sihit_startPosZ.at(pos)),fabs(sihit_endPosZ.at(pos)));
		double z_entry = sihit_startPosZ.at(pos)/fabs(sihit_startPosZ.at(pos)) * z;
		double z_exit  = sihit_endPosZ.at(pos)/fabs(sihit_endPosZ.at(pos)) * z;

		// local X correction
		double m = (sihit_startPosZ.at(pos) - sihit_endPosZ.at(pos))/(sihit_startPosX.at(pos) - sihit_endPosX.at(pos));
		double q = sihit_startPosZ.at(pos) - m* sihit_startPosX.at(pos);

		double entry = (z_entry -q)/m;
		double exit  = (z_exit  -q)/m;

		truth_locX = 0.5*(entry+exit);

		// local Y correction
		m = (sihit_startPosZ.at(pos) - sihit_endPosZ.at(pos))/(sihit_startPosY.at(pos) - sihit_endPosY.at(pos));
		q = sihit_startPosZ.at(pos) - m*sihit_startPosY.at(pos);

		entry = (z_entry -q)/m;
		exit  = (z_exit  -q)/m;

		truth_locY = 0.5*(entry+exit);

	      }

	    }


	    if( msos->detType() == Trk::TrackState::Pixel ){

	      _pixCluster_truth_barcode.push_back(*barcode);
	      _pixCluster_truth_locX.push_back(truth_locX);
	      _pixCluster_truth_locY.push_back(truth_locY);
	      _pixCluster_truth_reco_index.push_back(_pixCluster_truth_size.size()-1);

	    }

	    else if( msos->detType() == Trk::TrackState::SCT ){

	      _stripCluster_truth_barcode.push_back(*barcode);
	      _stripCluster_truth_locX.push_back(truth_locX);
	      _stripCluster_truth_locY.push_back(truth_locY);
	      _stripCluster_truth_reco_index.push_back(_pixCluster_truth_size.size()-1);

	    }

	  } //barcode loop

	}

      } // MSOS loop


      vector<float> residualLocX, residualLocY, measurementLocX, measurementLocY, trackParamLocX, trackParamLocY, angle, loceta, measurementLocCovX, measurementLocCovY;
      vector<int> phiWidth, etaWidth, measurement_region, measurement_det, measurement_iLayer, measurement_type;
      vector<uint64_t> pixelID;
      
      if(thisTrack->isAvailable< vector<float> >("hitResiduals_residualLocX")){
	residualLocX = thisTrack->auxdata< vector<float> >("hitResiduals_residualLocX");
	residualLocY = thisTrack->auxdata< vector<float> >("hitResiduals_residualLocY");
	phiWidth = thisTrack->auxdata< vector<int> >("hitResiduals_phiWidth");
	etaWidth = thisTrack->auxdata< vector<int> >("hitResiduals_etaWidth");
	measurement_region = thisTrack->auxdata< vector<int> >("measurement_region");
	measurement_det = thisTrack->auxdata< vector<int> >("measurement_det");
	measurement_iLayer = thisTrack->auxdata< vector<int> >("measurement_iLayer");
	measurement_type = thisTrack->auxdata< vector<int> >("measurement_type");
	if(thisTrack->isAvailable< vector<float> >("measurementLocX")){
	  measurementLocX = thisTrack->auxdata< vector<float> >("measurementLocX");
	  measurementLocY = thisTrack->auxdata< vector<float> >("measurementLocY");
	  trackParamLocX = thisTrack->auxdata< vector<float> >("trackParamLocX");
	  trackParamLocY = thisTrack->auxdata< vector<float> >("trackParamLocY");
	}
	if(thisTrack->isAvailable< vector<float> >("angle"))
	  angle = thisTrack->auxdata< vector<float> >("angle");
	if(thisTrack->isAvailable< vector<float> >("etaloc"))
	  loceta = thisTrack->auxdata< vector<float> >("etaloc");

	if(thisTrack->isAvailable< vector<float> >("measurementLocCovX")){
	  measurementLocCovX = thisTrack->auxdata< vector<float> >("measurementLocCovX");
	  measurementLocCovY = thisTrack->auxdata< vector<float> >("measurementLocCovY");
	}
	if(thisTrack->isAvailable< vector<uint64_t> >("pixelID"))
	   pixelID = thisTrack->auxdata< vector<uint64_t> >("pixelID");
      }

      for(unsigned int i_hit=0; i_hit<residualLocX.size(); i_hit++){

	_hitDeco_track_index.push_back(itrack);
	_hitDeco_residualLocX.push_back(residualLocX[i_hit]);
	_hitDeco_residualLocY.push_back(residualLocY[i_hit]);
	_hitDeco_phiWidth.push_back(phiWidth[i_hit]);
	_hitDeco_etaWidth.push_back(etaWidth[i_hit]);
	_hitDeco_measurement_region.push_back(measurement_region[i_hit]);
	_hitDeco_measurement_det.push_back(measurement_det[i_hit]);
	_hitDeco_measurement_iLayer.push_back(measurement_iLayer[i_hit]);
	_hitDeco_measurement_type.push_back(measurement_type[i_hit]);

	if(measurementLocX.size()>0){
	  _hitDeco_measurementLocX.push_back(measurementLocX[i_hit]);
	  _hitDeco_measurementLocY.push_back(measurementLocY[i_hit]);
	  _hitDeco_trackParamLocX.push_back(trackParamLocX[i_hit]);
	  _hitDeco_trackParamLocY.push_back(trackParamLocY[i_hit]);
	}

	if(angle.size()>0)
	  _hitDeco_angle.push_back(angle[i_hit]);
	if(loceta.size()>0)
	  _hitDeco_loceta.push_back(loceta[i_hit]);

	if(pixelID.size()>0)
	  _hitDeco_pixelID.push_back(pixelID[i_hit]);

	if(measurementLocCovX.size()>0){
	  _hitDeco_measurementLocCovX.push_back(measurementLocCovX[i_hit]);
	  _hitDeco_measurementLocCovY.push_back(measurementLocCovY[i_hit]);
	}

      }      

    }


    for(unsigned int i_pix=0; i_pix<_pixCluster_pixelID.size(); i_pix++){

      int i_hit_match = -1;

      for(unsigned int i_hit=0; i_hit<_hitDeco_pixelID.size(); i_hit++){
	if(_pixCluster_pixelID[i_pix]==_hitDeco_pixelID[i_hit]
	   && _pixCluster_track_index[i_pix]==_hitDeco_track_index[i_hit]){
	  i_hit_match = i_hit;
	  break;
	}
      }

      _pixCluster_hitDeco_index.push_back(i_hit_match);

    }

    for(unsigned int i_strip=0; i_strip<_stripCluster_stripID.size(); i_strip++){

      int i_hit_match = -1;

      // Despite the name, the hitDeco_pixelID branch stores the stripID in case of strip hits
      for(unsigned int i_hit=0; i_hit<_hitDeco_pixelID.size(); i_hit++){
	if(_stripCluster_stripID[i_strip]==_hitDeco_pixelID[i_hit]
	   && _stripCluster_track_index[i_strip]==_hitDeco_track_index[i_hit]){
	  i_hit_match = i_hit;
	  break;
	}
      }

      _stripCluster_hitDeco_index.push_back(i_hit_match);

    }

  }


  m_myTree->Fill();

  setFilterPassed(true); //if got here, assume that means algorithm passed
  return StatusCode::SUCCESS;
}




void TRKNtupleAlg::clean() { 

  _EventNumber = 0;

  _track_n = 0;
  _track_isLoose.clear();
  _track_isTightPrimary.clear();
  _track_isInOut.clear();

  _track_charge.clear();
  _track_pt.clear();
  _track_eta.clear();
  _track_theta.clear();
  _track_phi.clear();
  _track_phi0.clear();
  _track_d0.clear();
  _track_z0.clear();
  _track_z0st.clear();
  _track_qOverP.clear();
  _track_qOverPt.clear();

  _track_d0err.clear();
  _track_z0err.clear();
  _track_phierr.clear();
  _track_thetaerr.clear();
  _track_z0sterr.clear();
  _track_qOverPterr.clear();


  _track_prob.clear();

  _track_innermostPixLayerHits.clear();
  _track_innermostPixLayerOutliers.clear();
  _track_innermostPixLayerSplitHits.clear();
  _track_innermostPixLayerSharedHits.clear();
  _track_innermostExpectPixLayerHits.clear();

  _track_nextToInnermostPixLayerHits.clear();
  _track_nextToInnermostPixLayerOutliers.clear();
  _track_nextToInnermostPixLayerSplitHits.clear();
  _track_nextToInnermostPixLayerSharedHits.clear();
  _track_nextToInnermostExpectPixLayerHits.clear();

  _track_pixHits.clear();
  _track_pixHoles.clear();
  _track_pixSharedHits.clear();
  _track_pixOutliers.clear();
  _track_pixContribLayers.clear();
  _track_pixSplitHits.clear();
  _track_pixGangedHits.clear();
  _track_pixDead.clear();

  _track_SCTHits.clear();
  _track_SCTHoles.clear();
  _track_SCTDoubleHoles.clear();
  _track_SCTSharedHits.clear();
  _track_SCTOutliers.clear();
  _track_SCTDead.clear();

  _track_TRTHits.clear();
  _track_TRTHTHits.clear();
  _track_TRTOutliers.clear();
  _track_TRTHTOutliers.clear();

  _track_SiHits.clear();
  _track_SiSharedHits.clear();

  _track_fit_chi2.clear();
  _track_fit_ndf.clear();
  _track_fit_chi2ndf.clear();
  _track_fit_chi2prob.clear();

  _track_cov_matrix.clear();
  _track_cov_matrix_det.clear();

  _track_patternInfo.clear();

  _track_truth_index.clear();
  
  _truth_charge.clear();
  _truth_pt.clear();
  _truth_eta.clear();
  _truth_theta.clear();
  _truth_phi.clear();
  _truth_phi0.clear();
  _truth_mass.clear();
  _truth_d0.clear();
  _truth_z0.clear();
  _truth_qOverP.clear();
  _truth_qOverPt.clear();
  _truth_z0st.clear();
  _truth_prodR.clear();
  _truth_prodZ.clear();
  _truth_barcode.clear();
  _truth_isCharged.clear();
  _truth_status.clear();
  _truth_pdgId.clear();
  _truth_hasGrandParent.clear();
  _truth_eleFromGamma.clear();
  _truth_hasProdVtx.clear();
  _truth_isFromHSProdVtx.clear();
  _truth_prodVtxRadius.clear();
  _truth_prodVtxZ.clear();
  _truth_hasDecayVtx.clear();
  _truth_decayVtxRadius.clear();
  _truth_decayVtxZ.clear();
  _truth_StdSel.clear();
  _truth_StdSel_ITk.clear();

  _truth_isFromSUSY.clear();
  _truth_SUSY_index.clear();

  _truth_SUSY_deta.clear();
  _truth_SUSY_dphi.clear();
  _truth_PVDV_deta.clear();
  _truth_PVDV_dphi.clear();

  _truth_track_index.clear();

  _truth_parent_index.clear();
  _truth_child_index.clear();
  _truth_n_children.clear();

  _truth_isFromTau.clear();
  _truth_tau_parent_index.clear();
  _truth_isFromB.clear();
  _truth_B_parent_index.clear();

  _gen_vtx_n = -1;
  _gen_vtx_x = -1e9;
  _gen_vtx_y = -1e9;
  _gen_vtx_z = -1e9;
  _gen_vtx_delta_z_closest = -1e9;

  _gen_vertices_x.clear();
  _gen_vertices_y.clear();
  _gen_vertices_z.clear();
  _gen_vertices_isHS.clear();

  _reco_vtx_n = -1;
  _reco_vtx_x = -1e9;
  _reco_vtx_y = -1e9;
  _reco_vtx_z = -1e9;

  _reco_vertices_x.clear();
  _reco_vertices_y.clear();
  _reco_vertices_z.clear();

  _mu = -1;

  _pixCluster_track_index.clear();
  _pixCluster_reco_globX.clear();
  _pixCluster_reco_globY.clear();
  _pixCluster_reco_globZ.clear();
  _pixCluster_reco_globR.clear();
  _pixCluster_reco_globEta.clear();
  _pixCluster_reco_globPhi.clear();
  _pixCluster_reco_locX.clear();
  _pixCluster_reco_locY.clear();
  _pixCluster_bec.clear();
  _pixCluster_isInclined.clear();
  _pixCluster_layer.clear();
  _pixCluster_sizePhi.clear();
  _pixCluster_sizeZ.clear();
  _pixCluster_localTheta.clear();
  _pixCluster_omegax.clear();
  _pixCluster_omegay.clear();
  _pixCluster_LorentzShift.clear();
  _pixCluster_etaModule.clear();
  _pixCluster_phiModule.clear();
  _pixCluster_etaIndex.clear();
  _pixCluster_phiIndex.clear();
  _pixCluster_charge.clear();
  _pixCluster_ToT.clear();
  _pixCluster_centroid_xeta.clear();
  _pixCluster_centroid_xphi.clear();
  _pixCluster_pixelID.clear();
  _pixCluster_waferID.clear();
  _pixCluster_hitDeco_index.clear();

  _pixCluster_truth_size.clear();
  _pixCluster_truth_locX.clear();
  _pixCluster_truth_locY.clear();
  _pixCluster_truth_barcode.clear();
  _pixCluster_truth_reco_index.clear();

  _hitDeco_track_index.clear();
  _hitDeco_measurement_region.clear();
  _hitDeco_measurement_det.clear();
  _hitDeco_measurement_iLayer.clear();
  _hitDeco_measurement_type.clear();
  _hitDeco_residualLocX.clear();
  _hitDeco_residualLocY.clear();
  _hitDeco_phiWidth.clear();
  _hitDeco_etaWidth.clear();
  _hitDeco_measurementLocX.clear();
  _hitDeco_measurementLocY.clear();
  _hitDeco_trackParamLocX.clear();
  _hitDeco_trackParamLocY.clear();
  _hitDeco_measurementLocCovX.clear();
  _hitDeco_measurementLocCovY.clear();
  _hitDeco_angle.clear();
  _hitDeco_loceta.clear();
  _hitDeco_pixelID.clear();

  _stripCluster_track_index.clear();
  _stripCluster_reco_globX.clear();
  _stripCluster_reco_globY.clear();
  _stripCluster_reco_globZ.clear();
  _stripCluster_reco_globR.clear();
  _stripCluster_reco_globEta.clear();
  _stripCluster_reco_globPhi.clear();
  _stripCluster_reco_locX.clear();
  _stripCluster_reco_locY.clear();
  _stripCluster_bec.clear();
  _stripCluster_layer.clear();
  _stripCluster_siWidth.clear();
  _stripCluster_etaModule.clear();
  _stripCluster_phiModule.clear();
  _stripCluster_stripID.clear();
  _stripCluster_side.clear();
  _stripCluster_strip.clear();
  _stripCluster_row.clear();
  _stripCluster_hitDeco_index.clear();

  _stripCluster_truth_size.clear();
  _stripCluster_truth_locX.clear();
  _stripCluster_truth_locY.clear();
  _stripCluster_truth_barcode.clear();
  _stripCluster_truth_reco_index.clear();
  
} 
