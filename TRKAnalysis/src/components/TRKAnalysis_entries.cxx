
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../TRKNtupleAlg.h"

DECLARE_ALGORITHM_FACTORY( TRKNtupleAlg )

DECLARE_FACTORY_ENTRIES( TRKAnalysis ) 
{
  DECLARE_ALGORITHM( TRKNtupleAlg );
}
