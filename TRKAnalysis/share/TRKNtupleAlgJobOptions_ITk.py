#Skeleton joboption for a simple analysis job

#---- Minimal job options -----

import AthenaPoolCnvSvc.ReadAthenaPool

runDAOD = False # these are DAOD-s

# make AthenaCommonFlags aware of which file we are using
# AthenaCommonFlags are used run-time configuration (InputFilePeeker)
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags

if len(athenaCommonFlags.FilesInput.get_Value()) == 0:
   athenaCommonFlags.FilesInput = FNAME
#-----------------------------------------------------------------------

if "DAOD_IDTRKVALID" in svcMgr.EventSelector.InputCollections[0]:
      print "Set up for DAOD processing"
      runDAOD=True
#-----------------------------------------------------------------------

# pre- and post- includes files needed to read input files
from AthenaCommon.GlobalFlags import globalflags

from RecExConfig.InputFilePeeker import inputFileSummary
globalflags.DataSource = 'data' if inputFileSummary['evt_type'][0] == "IS_DATA" else 'geant4'
globalflags.DetDescrVersion = inputFileSummary['geometry']

# New geotags do not need any special pre- and post-includes
# This xmlTags list is kept for backward compatibility and should not be updated
xmlTags = [
            # step 1.7
            ["ATLAS-P2-ITK-09","ExtBrl_4",""],
            ["ATLAS-P2-ITK-10","InclBrl_4",""],
            # step 1.8 and step 1.9, nominal (run4) beam-pipe
            # step 1.9, run2 beam-pipe
            ["ATLAS-P2-ITK-13","InclBrl_4_OptRing",""],
            # step 3
            ["ATLAS-P2-ITK-17","InclBrl_4","InclinedAlternative"],
            # step 2
            ["ATLAS-P2-ITK-19","InclBrl_4","InclinedQuads"],
            ["ATLAS-P2-ITK-20","InclBrl_4","InclinedDuals"],
            # step 3.1
            ["ATLAS-P2-ITK-22-00","InclBrl_4","InclinedAlternative"],
            ["ATLAS-P2-ITK-22-01","InclBrl_4","InclinedAlternative"],
            ["ATLAS-P2-ITK-22-02","",""],
            ]

from InDetSLHC_Example.SLHC_JobProperties import SLHC_Flags

foundGeoTag = False
for geoTag, layoutDescr, layoutOption in xmlTags:
   if (globalflags.DetDescrVersion().startswith(geoTag)):
      foundGeoTag = True
      print "preIncludes for ",layoutDescr, " layout"
      if (layoutOption!=""): 
         SLHC_Flags.LayoutOption=layoutOption
      from InDetRecExample.InDetJobProperties import InDetFlags
      if (layoutDescr!=""):
         include('InDetSLHC_Example/preInclude.SLHC_Setup_'+layoutDescr+'.py')
      else:
         include("InDetSLHC_Example/preInclude.SLHC_Setup.py")
      include('InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py')
      include('InDetSLHC_Example/preInclude.SLHC.SiliconOnly.Reco.py')
      break

if(not foundGeoTag):
   from InDetRecExample.InDetJobProperties import InDetFlags
   include("InDetSLHC_Example/preInclude.SLHC_Setup.py")
   include('InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py')
   include('InDetSLHC_Example/preInclude.SLHC.SiliconOnly.Reco.py')

   
# Just turn on the detector components we need
from AthenaCommon.DetFlags import DetFlags
DetFlags.detdescr.all_setOff() 
DetFlags.detdescr.SCT_setOn() 
DetFlags.detdescr.BField_setOn() 
DetFlags.detdescr.pixel_setOn() 

# Set up geometry and BField 
include("RecExCond/AllDet_detDescr.py")

SLHC_Flags.SLHC_Version = ''

         
from AthenaCommon.GlobalFlags import jobproperties
DetDescrVersion = jobproperties.Global.DetDescrVersion()

for geoTag, layoutDescr, layoutOption in xmlTags:
   if (globalflags.DetDescrVersion().startswith(geoTag)):
      if(layoutDescr!=""):
         print "postInclude for ",layoutDescr, " layout"
         include('InDetSLHC_Example/postInclude.SLHC_Setup_'+layoutDescr+'.py')
      else:
         include('InDetSLHC_Example/postInclude.SLHC_Setup_ITK.py')
      break

if(not foundGeoTag):
   include('InDetSLHC_Example/postInclude.SLHC_Setup_ITK.py')


#-----------------------------------------------------------------------

# Access the algorithm sequence:
from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

# include xml file with histogram definitions 
from InDetPhysValMonitoring.InDetPhysValMonitoringConf import HistogramDefinitionSvc
ToolSvc = ServiceMgr.ToolSvc
ServiceMgr+=HistogramDefinitionSvc()
ServiceMgr.HistogramDefinitionSvc.DefinitionSource="../share/InDetPVMPlotDefITK.xml"
ServiceMgr.HistogramDefinitionSvc.DefinitionFormat="text/xml"

# this fills some extra histograms when not running over DAOD
# when running over DAOD, decorators should be off to prevent crashes
if not runDAOD : 

  from InDetPhysValMonitoring.InDetPhysValMonitoringConf import InDetPhysValDecoratorAlg
  decorators = InDetPhysValDecoratorAlg()
  if(foundGeoTag):
    decorators.InDetPhysHitDecoratorTool.UseNewITkLayerNumbering = False
  topSequence += decorators



jps.AthenaCommonFlags.HistOutputs = ["MYSTREAM:TRK_ntuple.root"]  #register output files like this. MYSTREAM is used in the code


topSequence += CfgMgr.TRKNtupleAlg(IsHitInfoAvailable=False)  


#---- Options you could specify on command line -----
jps.AthenaCommonFlags.EvtMax=-1                          #set on command-line with: --evtMax=-1
#jps.AthenaCommonFlags.SkipEvents=0                       #set on command-line with: --skipEvents=0
#jps.AthenaCommonFlags.FilesInput = ["/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/CommonInputs/DAOD_PHYSVAL/mc16_13TeV.410501.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.DAOD_PHYSVAL.e5458_s3126_r9364_r9315_AthDerivation-21.2.1.0.root"]        #set on command-line with: --filesInput=...
jps.AthenaCommonFlags.FilesInput = ["AOD.pool.root"]


include("AthAnalysisBaseComps/SuppressLogging.py")              #Optional include to suppress as much athena output as possible. Keep at bottom of joboptions so that it doesn't suppress the logging of the things you have configured above

