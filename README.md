**Installation instructions**

```mkdir myProject
cd myProject
setupATLAS
lsetup git
git clone https://gitlab.cern.ch/tstreble/trkanalysis.git source
mkdir build
cd build
acmSetup Athena,21.9,latest #To change if needed
acm compile
mkdir run
cd run/
athena TRKAnalysis/TRKNtupleAlgJobOptions.py #Adapt input files if needed
```

**Grid submission**

Move submitJobs.py in run and adapt script (suffix,username,dsList,nFilesPerJob,destSE)

```cd build
setupATLAS
acmSetup
lsetup rucio
lsetup panda
voms-proxy-init -voms atlas #Password needed
cd run
python submitJobs.py
