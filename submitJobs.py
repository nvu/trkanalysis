import os
import sys

suffix="_ttbar_PU200_r11148_191126"
username="tstreble"

dsList=open("mc_samples_ttbar_PU200_r11148.txt",'r')
lines=dsList.readlines()

def submitJob(ds) :
    com = "pathena TRKAnalysis/TRKNtupleAlgJobOptions.py "

    com += "--inDS " + ds + " "
    oDS = "user."+username+"."+ds.replace("/","")+suffix

    print oDS+"  has length: "+str(len(oDS))
    while len(oDS) > 115 :
        print len(oDS)," too long!!!"
        splODS=oDS.split("_")
        splODS.pop(2)
        oDS="_".join(splODS)
        pass
    print "final: "+oDS+"  has length: "+str(len(oDS))
    com += "--outDS "+ oDS + " "
  
    com += "--nFilesPerJob 1 "

    com += "--destSE IN2P3-CPPM_SCRATCHDISK "
    
    return com



for ds in lines :
    if "#" in ds : continue    
    print " "
    print "///////////////////////////////////////////////////////////////////////////////////////"
    print ds
    command=submitJob(ds.replace("\n",""))
    print command
    os.system(command)
    pass
